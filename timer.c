#include <poll.h>
#include <pthread.h>
#include <signal.h>

#define TIMER_BASE_UNIT HOURS
#define TIMER_MARKER_CMD_FMT "yad --entry --fixed --title \"Edit Mark\" --text \"%s\" --splash 2> /dev/null"
#define TIMER_UNNAMED_MARK_FMT "mark %u"

volatile sig_atomic_t timer_active;

struct termios tio,
               tio_old;

FILE *log_fh;
size_t status_np;
pid_t alert_pid;

enum {
    INPUT_NONE,
    INPUT_WAITING,
    INPUT_SET
};

struct {
    char *time_format,
         *diff_unit_delim;
    pthread_t thr;
    FILE *p;
    unsigned int input;
    ptime_t time;
    char info_str[256],
         deferred_msg[256];
    size_t count;
} marker;

inline static
void clear_status(void) {
    size_t i;

    fwrite("\r", 1, 1, stderr);
    for(i=0; i < status_np; ++i)
        fwrite(" ", 1, 1, stderr);
    fwrite("\r", 1, 1, stderr);
    fflush(stderr);

    status_np = 0;
}

inline static
void print_mark(char *msg_fmt, ...) {
    va_list ap;
    char msg_buf[256],
         *msg_src,
         line[512];
    size_t n;

    if(msg_fmt != NULL) {
        va_start(ap, msg_fmt);
        vsnprintf(msg_buf, sizeof(msg_buf), msg_fmt, ap);
        va_end(ap);

        msg_src = msg_buf;
    }
    else {
        msg_src = marker.deferred_msg;
    }

    clear_status();

    n = snprintf(line, sizeof(line), "%s | %s\n", marker.info_str, msg_src);
    if(log_fh != NULL)
        fwrite(line, 1, n, log_fh);

    printf("%s", line);
    fflush(stdout);
}

void * marker_thread(void *arg) {
    char marker_cmd[256];
    int status;
    size_t nr,
           len;

    snprintf(marker_cmd, sizeof(marker_cmd), TIMER_MARKER_CMD_FMT, marker.info_str);

    marker.p = popen(marker_cmd, "r");
    if(marker.p == NULL) {
        clear_status();
        fprintf(stderr, "\e[31merror: \e[91mcannot execute marker command\e[0m\n");
        return NULL;
    }

    if(wait(&status) != -1) {
        if(status == 0) {
            memset(marker.deferred_msg, 0, sizeof(marker.deferred_msg));
            nr = fread(marker.deferred_msg, 1, sizeof(marker.deferred_msg), marker.p);

            if(nr > 0 && (len=nr-1) > 0) {
                if(marker.deferred_msg[len] != '\0')
                    marker.deferred_msg[len] = '\0';

                marker.input = INPUT_SET;
            }
            else {
                // zero length input, print unnamed mark
                print_mark(TIMER_UNNAMED_MARK_FMT, ++marker.count);
                marker.input = INPUT_NONE;
            }
        }
        else {
            marker.input = INPUT_NONE;
        }
    }

    fclose(marker.p);
    marker.p = NULL;

    return NULL;
}

inline static
void setup_mark(bool with_diff, bool dynamic) {
    time_t mt;
    struct tm mts;
    struct time_diff ds;
    char mark_time[256],
         mark_diff[128];

    memset(&mts, 0, sizeof(mts));
    memset(marker.info_str, 0, sizeof(marker.info_str));

    mt = time(NULL);
    localtime_r(&mt, &mts);
    strftime(mark_time, sizeof(mark_time), marker.time_format, &mts);

    if(with_diff) {
        time_parse_diff(marker.time, &mts, TIMER_BASE_UNIT, &ds);
        time_snprint_diff(mark_diff, sizeof(mark_diff), marker.diff_unit_delim, &ds, true, true);

        snprintf(marker.info_str, sizeof(marker.info_str), "%s | %s", mark_time, mark_diff);
    }
    else {
        snprintf(marker.info_str, sizeof(marker.info_str), "%s", mark_time);
    }

    if(dynamic) {
        marker.input = INPUT_WAITING;
        pthread_create(&marker.thr, NULL, marker_thread, NULL);
    }
}

inline static
void start_expiry_alert(char **alert_argv) {
    alert_pid = fork();

    if(alert_pid == -1) {
        clear_status();
        fprintf(stderr, "\e[31merror: \e[91mcannot fork for alert command\e[0m\n");
        exit(EXIT_FAILURE);
    }

    if(alert_pid == 0 && execvp(alert_argv[0], alert_argv) == -1) {
        clear_status();
        fprintf(stderr, "\e[31merror: \e[91mcannot execute alert command\e[0m\n");
        exit(EXIT_FAILURE);
    }
}

inline static
void stop_expiry_alert() {
    if(alert_pid != -1) {
        kill(alert_pid, SIGTERM);
        alert_pid = -1;
    }
}

void on_sigterm(int signal) {
    clear_status();
    timer_active = false;
    setup_mark(true, false);
    print_mark("process killed");

    if(log_fh != NULL)
        fclose(log_fh);
}

int timer_main(int mode, bool autostart, time_t init_value, char *log_file, char *log_session_name,
               char *adj_str, char *time_format, char *diff_unit_delim, char **alert_argv)
{
    time_t lt;
    struct tm lts;
    char ctbuf[256],
         log_delim[512];

    ptime_t adj;
    struct sigaction sa;
    pid_t logger_pid;

    // input polling
    struct pollfd p;
    char c;

    // local timer state
    bool clean,
         paused,
         expired;
    ptime_t heap,
            base,
            now,
            diff,
            actual;

    // status
    struct time_diff ds;
    char *status_prefix,
         status[256],
         value_str[128];
    size_t i,j;

    #define make_active_mark() { \
        now = time_now(CLOCK_MONOTONIC); \
        if(mode == MODE_TIMER) \
            marker.time = heap-(now-base); \
        else \
            marker.time = heap+(now-base); \
    }

    #define make_mark() { \
        if(paused) { \
            marker.time = heap; \
        } \
        else { \
            make_active_mark(); \
        } \
    }

    if(log_file != NULL) {
        log_fh = fopen(log_file, "a");
        if(log_fh == NULL) {
            fprintf(stderr, "\e[31merror: \e[91cannot open log file\e[0m\n");
            return 1;
        }

        lt = time(NULL);
        localtime_r(&lt, &lts);

        if(strftime(ctbuf, sizeof(ctbuf), output_time_format, &lts) == 0) {
            fprintf(stderr, "\e[31merror: cannot convert time result\e[0m\n");
            return 1;
        }

        if(log_session_name != NULL)
            snprintf(log_delim, sizeof(log_delim), "---- %s @ %s ----\n", log_session_name, ctbuf);
        else
            snprintf(log_delim, sizeof(log_delim), "---- %s ----\n", ctbuf);

        if(ftell(log_fh) != 0)
            fwrite("\n", 1, 1, log_fh);

        fwrite(log_delim, 1, strlen(log_delim), log_fh);
    }
    else {
        log_fh = NULL;
    }

    if(time_parse_offset(adj_str, NULL, &adj) != 0) {
        fprintf(stderr, "\e[31merror: \e[91minvalid timer adjustment\e[0m\n");
        if(log_fh != NULL)
            fclose(log_fh);

        return 1;
    }

    // SIGKILL handler
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = on_sigterm;
    sigaction(SIGTERM, &sa, NULL);

    // save current termio settings
    tcgetattr(STDIN_FILENO, &tio);
    memcpy(&tio_old, &tio, sizeof(tio));
    // disable echo, enable canonical mode, buffer 1 char at a time
    tio.c_lflag &= ~(ECHO|ICANON);
    tio.c_cc[VMIN] = 1;
    tcsetattr(STDIN_FILENO, TCSANOW, &tio);

    marker.time_format = time_format;
    marker.diff_unit_delim = diff_unit_delim;
    marker.input = INPUT_NONE;
    marker.count = 0;

    if(autostart) {
        base = time_now(CLOCK_MONOTONIC);

        marker.time = init_value;
        setup_mark(true, false);
        print_mark("autostart");

        clean = false;
        paused = false;
    }
    else {
        clean = true;
        paused = true;
    }

    status_np = 0;
    alert_pid = -1;

    heap = init_value;
    actual = heap;
    expired = false;

    p.fd = STDIN_FILENO;
    p.events = POLLIN;

    timer_active = true;
    while(timer_active) {
        if(poll(&p, 1, paused ? 50 : 10) == 1) {
            fread(&c, 1, 1, stdin);

            // toggle
            if(c == ' ') {
                // resume
                if(paused) {
                    base = time_now(CLOCK_MONOTONIC);

                    marker.time = heap;
                    setup_mark(true, false);
                    print_mark(actual == init_value ? "start" : "resume");

                    paused = false;
                }
                // pause
                else {
                    now = time_now(CLOCK_MONOTONIC);

                    // adjust heap from elapsed time
                    diff = now-base;
                    if(mode == MODE_TIMER)
                        heap -= diff;
                    else
                        heap += diff;

                    marker.time = heap;
                    setup_mark(true, false);
                    print_mark("pause");

                    paused = true;
                }
            }
            // decrement
            else if(c == '-') {
                make_mark();
                setup_mark(paused == false, false);
                print_mark("adjustment: -%s", adj_str);

                heap -= adj;
            }
            // increment
            else if(c == '+') {
                make_mark();
                setup_mark(paused == false, false);
                print_mark("adjustment: +%s", adj_str);

                heap += adj;
                if(heap > 0) {
                    stop_expiry_alert();
                    expired = false;
                }
            }
            // mark
            else if(c == 'm') {
                make_mark();
                setup_mark(paused == false, false);
                print_mark(TIMER_UNNAMED_MARK_FMT, ++marker.count);
            }
            // mark with message
            else if(c == 'M') {
                if(marker.input == INPUT_NONE) {
                    make_mark();
                    setup_mark(paused == false, true);
                }
            }
            // reset
            else if(c == 'r') {
                if(actual != init_value) {
                    make_mark();
                    setup_mark(paused == false, false);
                    print_mark("reset");

                    paused = true;
                    if(mode == MODE_TIMER) {
                        stop_expiry_alert();
                        expired = false;
                    }

                    heap = init_value;
                }
            }
            // quit
            else if(c == 'q') {
                make_mark();
                setup_mark(paused == false, false);
                print_mark("%s", "quit");

                timer_active = 0;
                break;
            }
        }

        if(marker.input == INPUT_SET) {
            print_mark(NULL); // NULL = use marker.deferred_msg
            marker.input = INPUT_NONE;
        }

        if(paused) {
            actual = heap;

            if(clean) {
                if(mode == MODE_STOPWATCH)
                    status_prefix = "\e[93mstopwatch\e[0m";
                else
                    status_prefix = "\e[93mcountdown\e[0m";
            }
            else {
                status_prefix = "\e[93mpaused\e[0m";
            }
        }
        else {
            clean = false;

            make_active_mark();
            actual = marker.time;

            if(mode == MODE_TIMER) {
                if(actual <= 0) {
                    status_prefix = "\e[91mexpired\e[0m";

                    if(expired == false) {
                        setup_mark(false, false);
                        print_mark("%s", "expiry");
                        fwrite("\a", 1, 1, stderr);

                        if(alert_argv != NULL)
                            start_expiry_alert(alert_argv);

                        expired = true;
                    }
                }
                else {
                    status_prefix = "\e[92mactive\e[0m";
                }
            }
            else { // if(mode == MODE_STOPWATCH)
                status_prefix = "\e[92mactive\e[0m";
            }
        }

        // calculate diff
        memset(&ds, 0, sizeof(struct time_diff));
        time_parse_diff(actual, NULL, TIMER_BASE_UNIT, &ds);
        time_snprint_diff(value_str, sizeof(value_str), marker.diff_unit_delim, &ds, true, false);

        // update status
        clear_status();
        status_np = fprintf(stderr, "[%s] %s", status_prefix, value_str);
        fflush(stderr);
    }

    clear_status();
    stop_expiry_alert();

    // restore termio settings
    tcsetattr(STDIN_FILENO, TCSANOW, &tio_old);

    // close log file
    if(log_fh != NULL)
        fclose(log_fh);

    return 0;
}
