#include <wat/time.h>
#include <wat/string.h>

#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <termios.h>
#include <unistd.h>

#ifdef DEBUG
#define debug(...) fprintf(stderr, __VA_ARGS__)
#else
#define debug(...)
#endif

enum {
    MODE_TIME,
    MODE_DIFF,
    MODE_TIMER,
    MODE_STOPWATCH
};

size_t unit_index(char *str) {
    size_t u;

    for(u=0; u < UNIT_MAX; ++u) {
        if(strcmp(str, time_units[u]) == 0)
            break;
    }

    return u;
}

int parse_time(char *str, char *time_format, ptime_t *t_ret, struct tm *ref_ret) {
    struct tm ts;
    ptime_t t,
            diff,
            f,i;
    time_t ct;
    char *last;

    if(strcmp(str, "now") == 0) {
        t = time_now(CLOCK_REALTIME);

        *t_ret = t;
        if(ref_ret != NULL) {
            ct = (time_t)t;
            localtime_r(&ct, &ts);
            *ref_ret = ts;
        }
    }
    else if(*str == '-' || *str == '+') {
        t = time_now(CLOCK_REALTIME);
        ct = (time_t)t;

        // apply offset

        localtime_r(&ct, &ts);
        if(time_parse_offset(str, &ts, &diff) != 0)
            return 1;

        t += diff;

        // return

        *t_ret = t;
        if(ref_ret != NULL) {
            ct = (time_t)t;
            localtime_r(&ct, &ts);
            *ref_ret = ts;
        }
    }
    else {
        memset(&ts, 0, sizeof(struct tm));

        ct = (time_t)time_now(CLOCK_REALTIME);
        localtime_r(&ct, &ts);

        last = strptime(str, time_format, &ts);
        if(last == NULL)
            return 1;

        // convert

        ts.tm_isdst = -1; // important
        t = mktime(&ts);

        // return

        *t_ret = t;
        if(ref_ret != NULL)
            *ref_ret = ts;
    }

    return 0;
}

// options

char        *input_time_format = "%y/%m/%d %H:%M:%S";
char        *output_time_format = "%y/%m/%d %H:%M:%S";
unsigned int diff_base_unit = YEARS;
char        *diff_unit_delim = "";
int          diff_rounding_precision = -1;
double       diff_rounding_threshold = 0.5;
char        *timer_adjustment = "30s";
bool         timer_autostart = false;
char        *timer_log_file = NULL;
char        *timer_log_session_name = NULL;
char        *timer_alert_cmd = NULL;

#include "timer.c"

int main(int argc, char **argv) {
    int i,j;
    unsigned int mode,
                 u;
    char *input_str,
         *aux_time_str,
         *offset_str,
         **timer_alert_argv;
    size_t timer_alert_argc;
    ptime_t s1,
            s2,
            s,
            offset,
            diff,
            timer_value;
    time_t ct;
    struct tm t1,
              t2,
              t;
    struct time_diff d;
    char rbuf[512];

    char *usage =
        "Usage: ora <input> [args ..]\n"
        "\n"
        " <input> is a time in time/diff mode and an offset in timer/stopwatch mode.\n"
        " Times may be specified using 'now' or with a relative offset (eg: 2d3h45m).\n"
        "\n"
        " -o    <offset>\n"
        "       Relative offset\n"
        " -d    <time>\n"
        "       Calculate diff\n"
        " -f    <format>\n"
        "       Input time format (default: '%y/%m/%d %H:%M:%S')\n"
        " -F    <format>\n"
        "       Output time format (default: '%y/%m/%d %H:%M:%S')\n"
        " -D    [y,M,d,h,m,s]\n"
        "       Base diff unit (default: y)\n"
        " -u    <str>\n"
        "       Diff unit delimiter\n"
        " -r    [dhms]\n"
        "       diff rounding precision\n"
        " -R    [0.0 - 1.0]\n"
        "       Diff rounding threshold (default: 0.5)\n"
        "\n"
        " ----\n"
        "\n"
        " -t    Timer mode\n"
        " -s    Stopwatch mode\n"
        " -a    Autostart\n"
        "\n"
        " -l    Log file\n"
        " -L    Log session name\n"
        " -j    <offset>\n"
        "       Adjustment offset (default: 30s)\n"
        " -x    <cmd>\n"
        "       Alert command\n"
        "\n"
        "       space  toggle\n"
        "       -      decrement time\n"
        "       +      increment time\n"
        "       m      mark\n"
        "       M      mark with message\n"
        "       r      reset\n"
        "       q      quit\n"
        "\n";

    mode = MODE_TIME;
    input_str = argv[1];
    offset_str = NULL;

    if(argc < 2) {
        fprintf(stderr, "%s", usage);
        return 1;
    }

    for(i=2,j=argc-1; i < argc; ++i) {
        if(strcmp(argv[i], "-o") == 0 && i < j) {
            offset_str = argv[++i];
        }
        else if(strcmp(argv[i], "-d") == 0 && i < j) {
            mode = MODE_DIFF;
            aux_time_str = argv[++i];
        }
        if(strcmp(argv[i], "-f") == 0) {
            if(i == j) {
                fprintf(stderr, "error: no format argument specified\n");
                return 1;
            }

            input_time_format = argv[++i];
        }
        else if(strcmp(argv[i], "-F") == 0) {
            if(i == j) {
                fprintf(stderr, "error: no format argument specified\n");
                return 1;
            }

            output_time_format = argv[++i];
        }
        else if(strcmp(argv[i], "-D") == 0 && i < j) {
            diff_base_unit = unit_index(argv[++i]);
        }
        else if(strcmp(argv[i], "-u") == 0 && i < j) {
            diff_unit_delim = argv[++i];
        }
        else if(strcmp(argv[i], "-r") == 0 && i < j) {
            u = unit_index(argv[++i]);
            if(u == -1) {
                fprintf(stderr, "\e[31merror: \e[91minvalid rounding precision\e[0m\n");
                return 1;
            }

            diff_rounding_precision = u;
        }
        else if(strcmp(argv[i], "-R") == 0 && i < j) {
            diff_rounding_threshold = strtod(argv[++i], NULL);
            if(diff_rounding_threshold == 0.0 || diff_rounding_threshold >= 1.0) {
                fprintf(stderr, "\e[31merror: \e[91minvalid rounding threshold\e[0m\n");
                return 1;
            }
        }
        else if(strcmp(argv[i], "-t") == 0) {
            mode = MODE_TIMER;
        }
        else if(strcmp(argv[i], "-s") == 0) {
            mode = MODE_STOPWATCH;
        }
        else if(strcmp(argv[i], "-a") == 0) {
            timer_autostart = true;
        }
        else if(strcmp(argv[i], "-l") == 0 && i < j) {
            timer_log_file = argv[++i];
        }
        else if(strcmp(argv[i], "-L") == 0 && i < j) {
            timer_log_session_name = argv[++i];
        }
        else if(strcmp(argv[i], "-j") == 0 && i < j) {
            timer_adjustment = argv[++i];
            while(ispunct(*timer_adjustment))
                ++timer_adjustment;
        }
        else if(strcmp(argv[i], "-x") == 0 && i < j) {
            timer_alert_cmd = argv[++i];
        }
    }

    if(mode == MODE_TIME) {
        debug("\e[37m>\e[0m \e[97mtime mode\e[0m\n");

        if(parse_time(input_str, input_time_format, &s1, &t1) != 0) {
            fprintf(stderr, "\e[31merror: \e[91minvalid input time\e[0m\n");
            return 1;
        }

        debug("s1=\e[97m%.03Lf\e[0m\n", s1);

        s2=s1;
        t2=t1;

        if(offset_str != NULL) {
            if(time_parse_offset(offset_str, &t2, &offset) != 0) {
                fprintf(stderr, "\e[31merror: \e[91minvalid offset\e[0m\n");
                return 1;
            }

            s2 += offset;
            ct = (time_t)s2;
            localtime_r(&ct, &t2);
        }

        debug("s2=\e[97m%.03Lf\e[0m\n", s2);

        if(strftime(rbuf, sizeof(rbuf), output_time_format, &t2) == 0) {
            fprintf(stderr, "\e[31merror: \e[91mcannot convert time result\e[0m\n");
            return 1;
        }

        printf("%s\n", rbuf);
    }
    else if(mode == MODE_DIFF) {
        debug("\e[37m>\e[0m \e[97mdiff mode\e[0m\n");

        if(parse_time(input_str, input_time_format, &s1, &t1) != 0) {
            fprintf(stderr, "\e[31merror: \e[91minvalid input time\e[0m\n");
            return 1;
        }

        debug("s1=\e[97m%.03Lf\e[0m\n", s1);

        if(parse_time(aux_time_str, input_time_format, &s2, &t2) != 0) {
            fprintf(stderr, "\e[31merror: \e[91minvalid diff time\e[0m\n");
            return 1;
        }

        debug("s2=\e[97m%.03Lf\e[0m\n", s2);

        if(s1 > s2) {
            s = s2;
            s2 = s1;
            s1 = s;

            t = t2;
            t2 = t1;
            t1 = t;
        }

        diff = s2-s1;

        if(offset_str != NULL) {
            if(time_parse_offset(offset_str, &t2, &offset) != 0) {
                fprintf(stderr, "\e[31merror: \e[91minvalid offset\e[0m\n");
                return 1;
            }

            debug("offset=\e[97m%.03Lf\e[0m\n", offset);
            diff += offset;
        }

        debug("diff=\e[97m%.03Lf\e[0m\n", diff);

        if(diff_rounding_precision != -1) {
            time_round(&diff, diff_rounding_precision, diff_rounding_threshold);
            debug("diff(rounded)=\e[97m%.03Lf\e[0m\n", diff);
        }

        time_parse_diff(diff, &t2, diff_base_unit, &d);

        memset(rbuf, 0, sizeof(rbuf));
        time_snprint_diff(rbuf, sizeof(rbuf), diff_unit_delim, &d, true, true);

        printf("%s\n", rbuf);
    }
    else { // if(mode == MODE_TIMER || mode == MODE_STOPWATCH)
        if(timer_alert_cmd != NULL) {
            timer_alert_argv = strsplitq(timer_alert_cmd, " ", &timer_alert_argc);
            if(timer_alert_argv == NULL) {
                fprintf(stderr, "\e[31merror: \e[91out of memory\e[0m\n");
                return 1;
            }
        }
        else {
            timer_alert_argv = NULL;
        }

        if(time_parse_offset(input_str, NULL, &timer_value) != 0) {
            fprintf(stderr, "\e[31merror: \e[91minvalid input offset\e[0m\n");
            return 1;
        }

        if(timer_main(mode, timer_autostart, timer_value, timer_log_file, timer_log_session_name,
                      timer_adjustment, output_time_format, diff_unit_delim, timer_alert_argv) != 0)
        {
            return 1;
        }

        if(timer_alert_argv != NULL)
            strbuf_free(timer_alert_argv, timer_alert_argc);
    }

    return 0;
}
